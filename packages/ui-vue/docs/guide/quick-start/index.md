# 快速开始

## 开始使用 Farris UI Vue

### 1. 安装@farris/ui-vue

```
npm install @farris/ui-vue
```

```
yarn add @farris/ui-vue
```

### 2. 在应用中引入 Farris UI Vue

在`main.ts`文件中引入`@farris/ui-vue`。

```ts
import { createApp } from 'vue';
import App from './App.vue';
import Farris from '@farris/ui-vue';

createApp(App).use(Farris).mount('#app');
```

### 3. 在应用中使用 Farris UI Vue

在`App.vue`文件中使用 Farris UI Vue 组件。

```vue
<template>
    <f-input-group></f-input-group>
</template>
```
