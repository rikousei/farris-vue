# Button 按钮

Button 组件为不同使用场景提供了多种展示样式。

## 基本用法

:::demo

```vue
<script setup lang="ts">
import { ref } from 'vue';
</script>
<template>
    <f-button style="margin: 5px;"> 主要按钮 </f-button>
    <f-button style="margin: 5px;" :type="'danger'"> 危险按钮 </f-button>
    <f-button style="margin: 5px;" :type="'success'"> 成功按钮 </f-button>
    <f-button style="margin: 5px;" :type="'warning'"> 警告按钮 </f-button>
    <f-button style="margin: 5px;" :type="'secondary'"> 信息按钮 </f-button>
    <f-button style="margin: 5px;" :type="'link'"> 文本按钮 </f-button>
</template>
```

:::

## 状态

:::demo

```vue
<script setup lang="ts">
import { ref } from 'vue';
const disabled = ref(true);
</script>
<template>
    <div class="my-2 d-flex">
        <div class="f-demo-label">禁用按钮:</div>
        <f-switch v-model="disabled"></f-switch>
    </div>
    <div>
        <f-button style="margin: 5px;" :disabled="disabled"> 主要按钮 </f-button>
        <f-button style="margin: 5px;" :type="'danger'" :disabled="disabled"> 危险按钮 </f-button>
        <f-button style="margin: 5px;" :type="'success'" :disabled="disabled"> 成功按钮 </f-button>
        <f-button style="margin: 5px;" :type="'warning'" :disabled="disabled"> 警告按钮 </f-button>
        <f-button style="margin: 5px;" :type="'secondary'" :disabled="disabled"> 信息按钮 </f-button>
        <f-button style="margin: 5px;" :type="'link'" :disabled="disabled"> 文本按钮 </f-button>
    </div>
</template>
<style scoped>
.f-demo-label {
    line-height: 1.4rem;
    margin-right: 8px;
}
</style>
```

:::

## 尺寸

:::demo

```vue
<script setup lang="ts">
import { ref } from 'vue';
</script>
<template>
    <f-button style="margin: 5px;" :size="'small'"> 小尺寸 </f-button>
    <f-button style="margin: 5px;" :size="'large'"> 大尺寸 </f-button>
</template>
```

:::

## 按钮组

:::demo

```vue
<script setup lang="ts">
import { ref } from 'vue';

const data = ref([
    {
        id: 'b1',
        text: '增加',
        type: 'primary'
    },
    {
        id: 'b2',
        text: '删除',
        type: 'warning'
    },
    {
        id: 'b3',
        text: '保存',
        type: 'success'
    },
    {
        id: 'b4',
        text: '编辑',
        type: 'danger'
    }
]);
const data1 = ref([
    {
        id: 'b1',
        text: '增加',
        type: 'link'
    },
    {
        id: 'b2',
        text: '删除',
        type: 'link',
        disabled: true
    },
    {
        id: 'b3',
        text: '保存',
        type: 'link'
    },
    {
        id: 'b4',
        text: '编辑',
        type: 'link',
        disabled: true
    }
]);
</script>

<template>
    <div>
        <f-button-group :count="data.length" :data="data" :placement="'bottom-right'"></f-button-group>
        <f-button-group :count="2" :data="data1" :placement="'bottom-right'"></f-button-group>
    </div>
</template>

```

:::

## farris button
## 类型

```typescript
type ButtonType = 'primary' | 'warning' | 'danger' | 'success' | 'link' | 'secondary';
type SizeType = 'small' | 'large';
```
## 属性

| 属性名     | 类型                   | 默认值    | 说明                 |
| :--------- | :--------------------- | :-------- | :------------------- |
| id         | `string`               | --        | 标记组件的唯一标识   |
| type | `string as ButtonType` | 'primary' | 按钮显示样式         |
| disabled    | `boolean`              | false     | 将按钮设置为禁用状态 |
| size       | `string as SizeType`   | 'small'   | 按钮尺寸             |

## 事件

| 事件名 | 类型                | 说明         |
| :----- | :------------------ | :----------- |
| click  | `EventEmitter<any>` | 点击按钮事件 |

## farris button group
## 类型

```typescript
type PlacementDirection = 'top' | 'top-left' | 'top-right' | 'left' | 'left-top' | 'left-bottom' | 'bottom' | 'bottom-left' | 'bottom-right' | 'right' | 'right-top' | 'right-bottom';
```

## 属性

| 属性名    | 类型                           | 默认值   | 说明           |
| :-------- | :----------------------------- | :------- | :------------- |
| data      | `array`                        | --       | 按钮信息       |
| count     | `number`                       | 2        | 显示的按钮数量 |
| placement | `string as PlacementDirection` | 'bottom' | 按钮展示位置   |

## 事件

| 事件名      | 类型                    | 说明                                           |
| :---------- | :---------------------- | :--------------------------------------------- |
| changeState | `EventEmitter<boolean>` | 下拉框展开或者收起的回调，参数是下拉框是否展开 |
| change      | `EventEmitter<string>`  | 触发按钮的回调，参数是触发按钮的id             |

## 插槽

::: tip
暂无内容
:::
