# Avatar 头像

Avatar 组件用来展示人员头像。

## 基本用法

:::demo

```vue
<script setup lang="ts">
import { ref } from 'vue';
</script>
<template>
    <f-avatar></f-avatar>
</template>
```

:::

## 属性

| 属性名 | 类型     | 默认值 | 说明     |
| :----- | :------- | :----- | :------- |
| id     | `string` | --     | 组件标识 |

## 插槽

::: tip
暂无内容
:::
