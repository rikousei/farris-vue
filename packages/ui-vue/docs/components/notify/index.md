# Notify 通知消息

Notify 组件用来显示全局消息。

## 基本用法

:::demo

```vue
<script setup lang="ts">
import { ref, inject } from 'vue';

const NotifyService = inject('NotifyService');

function showMessage() {
    NotifyService.show({
        limit: 5,
        showCloseButton: false,
        position: 'top-center',
        timeout: 3000,
        theme: 'bootstrap',
        left: 0,
        right: 0,
        top: 0,
        bottom: 0,
        id: '1',
        animate: 'fadeIn',
        options: { type: 'string', title: 'This is a title', msg: 'This is some message' },
        toasts: [{ type: 'string', title: 'This is a title', msg: 'This is some message' }]
    });
}
</script>
<template>
    <button class="btn btn-primary" @click="showMessage">show notify</button>
</template>
```

:::

## 属性

| 属性名 | 类型     | 默认值 | 说明     |
| :----- | :------- | :----- | :------- |
| id     | `string` | --     | 组件标识 |

## 插槽

::: tip
暂无内容
:::
