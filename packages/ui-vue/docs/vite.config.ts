import { defineConfig } from 'vite';
import vueJsx from '@vitejs/plugin-vue-jsx';
import svgLoader from 'vite-svg-loader';

export default defineConfig({
    plugins: [vueJsx({}), svgLoader()],
    server: {
        fs: {
            strict: false
        }
    }
});
