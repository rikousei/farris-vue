---
layout: home
hero:
  name: Farris UI Vue
  text: 前端组件库
  tagline: 基于 Farris Design 具有 Fast Reliable Responsive Intuitive Smart 五大特性。
  actions:
    - theme: brand
      text: 快速开始
      link: /guide/quick-start/
    - theme: Gitee
      text: View on Gitee
      link: https://gitee.com/ubml/farris-vue
---
