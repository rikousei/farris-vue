import { computed, defineComponent, ref, SetupContext } from 'vue';
import { TabsProps, tabsProps } from './tabs.props';
import './tabs.css'
import { useTabs } from './composition/use-tabs';

export default defineComponent({
    name: 'FTabs',
    props: tabsProps,
    emits: [],
    setup(props: TabsProps, context: SetupContext) {
        const hideButtons = ref(true);

        const hideDropDown = ref(false);

        const hasInHeadClass = computed(() => {
            return false;
        });

        const fill = ref('');

        const tabType = ref('');

        const position = ref(props.position);

        const tabs = ref([] as any[]);

        const activeId = ref(props.activeId);

        const shouldShowNavFill = computed(() => {
            fill.value || tabType.value === 'fill';
        });

        const shouldShowNavPills = computed(() => { });

        const { setActiveId } = useTabs(props, context)

        const tabsHeaderClass = computed(() => ({
            'farris-tabs-header': true,
            'farris-tabs-inHead': hasInHeadClass.value,
            'farris-tabs-inContent': !hasInHeadClass.value,
            'farris-tabs-nav-fill': shouldShowNavFill.value,
            'farris-tabs-nav-pills': shouldShowNavPills.value
        }));

        const tabsTitleStyle = computed(() => ({
            width: hasInHeadClass.value ? (props.titleWidth ? `${props.titleWidth}%` : '') : ''
        }));

        const tabsTitleButtonClass = computed(() => ({
            btn: true,
            'sc-nav-btn': true,
            'px-1': true,
            'sc-nav-lr': true,
            'd-none': hideButtons.value
        }));

        const tabParentClass = computed(() => ({
            spacer: true,
            'f-utils-fill': true,
            'spacer-sides': !hideButtons.value && hideDropDown.value,
            'spacer-sides-dropdown': !hideButtons.value && !hideDropDown.value
        }));

        const tabContainerClass = computed(() => ({
            nav: true,
            'farris-nav-tabs': true,
            'flex-nowrap': true,
            'nav-fill': fill.value || tabType.value === 'fill',
            'nav-pills': tabType.value === 'pills',
            'flex-row': position.value === 'top' || position.value === 'bottom',
            'flex-column': position.value === 'left' || position.value === 'right'
        }));

        let tabPages = context.slots.default && context.slots.default()

        tabPages?.forEach((tabPage: any) => {
            tabs.value.push(tabPage.props)
        })

        setActiveId(tabs, activeId)

        const tabsContainerClass = computed(() => ({
            'farris-tabs': true,
            'farris-tabs-top': position.value === 'top',
            'farris-tabs-bottom': position.value === 'bottom',
            'farris-tabs-left': position.value === 'left',
            'farris-tabs-right': position.value === 'right'
        }))

        function getTabClass(tab: any) {
            return {
                'nav-item': true,
                'd-none': tab.show !== undefined ? !tab.show : false,
                'f-state-active': tab.id === activeId.value,
                'f-state-disable': tab.disabled,
            };
        }

        function getTabPageStyle(tabPage) {
            const props: any = tabPage.props
            return {
                display: props.id === activeId.value ? '' : 'none'
            }
        }

        function getTabStyle(tab: any) {
            return { width: `${tab.tabWidth}px` };
        }

        function getTabNavLinkClass(tab: any) {
            return {
                'nav-link': true,
                // 'tabs-text-truncate': true,
                active: tab.id === activeId.value,
                disabled: tab.disabled,
            };
        }

        function selectTabByIndex($event: Event, targetTabId: string) {
            activeId.value = targetTabId
            $event.preventDefault();
            $event.stopPropagation();
        }

        function getTabTextClass(tab: any) {
            return {
                'st-tab-text': true,
                'farris-title-auto': props.autoTitleWidth,
                'farris-title-text-custom': tab.titleOverflow
            };
        }

        function removeTab($event: Event, targetTabId: string) {
            tabs.value = tabs.value.filter(tab => tab.id !== targetTabId)
            tabPages = tabPages?.filter(tabPage => (tabPage.props as any).id !== targetTabId)
            if (activeId.value === targetTabId) {
                activeId.value = ''
                setActiveId(tabs, activeId)
            }
            $event.preventDefault();
            $event.stopPropagation();
        }

        return () => {
            return (
                <>
                    <div class={tabsContainerClass.value}>
                        <div class={tabsHeaderClass.value}>
                            <div class="farris-tabs-title scroll-tabs" style={tabsTitleStyle.value}>
                                <button type="button" class={tabsTitleButtonClass.value}></button>
                                <div class={tabParentClass.value} style="width:100%">
                                    <ul class={tabContainerClass.value}>
                                        {tabs.value.map((tab: any) => {
                                            return (
                                                <li class={getTabClass(tab)} style={getTabStyle(tab)}>
                                                    <a class={getTabNavLinkClass(tab)} onClick={($event) => selectTabByIndex($event, tab.id)}>
                                                        <span class={getTabTextClass(tab)}>{tab.title}</span>
                                                        {tab.removeable && (
                                                            <span class="st-drop-close" onClick={($event) => removeTab($event, tab.id)}>
                                                                <i class="f-icon f-icon-close"></i>
                                                            </span>
                                                        )}
                                                    </a>
                                                </li>
                                            );
                                        })}
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="farris-tabs-content">
                            {
                                tabPages?.map(tabPage => {
                                    return <div class="farris-tab-page" style={getTabPageStyle(tabPage)}>{tabPage}</div>
                                })
                            }
                        </div>
                    </div>
                </>
            );
        };
    }
});
