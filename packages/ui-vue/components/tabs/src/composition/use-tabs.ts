import { computed, ComputedRef, Ref, ref, SetupContext, watch } from 'vue';
import { TabsProps } from '../tabs.props';

export function useTabs(
    props: TabsProps,
    context: SetupContext
): any {
    
    function setActiveId(tabs: Ref<any[]>, activeId: Ref) {
        const index = tabs.value.findIndex(tab => tab.show !== false && !activeId.value && !tab.disabled)
        if (!activeId.value && index !== -1) {
            activeId.value = tabs.value[index].id
        }
    }
    
    return {
        setActiveId
    };
}
