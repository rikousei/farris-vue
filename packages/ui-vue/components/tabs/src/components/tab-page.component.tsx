import { defineComponent, SetupContext } from 'vue';
import { TabPageProps, tabPageProps } from './tab-page.props';

export default defineComponent({
    name: 'FTabPage',
    props: tabPageProps,
    emits: [],
    setup(props: TabPageProps, context: SetupContext) {
        return () => {
            return context.slots.default && context.slots.default();
        };
    }
});
