import { ExtractPropTypes } from 'vue';

export const tabPageProps = {
    tabWidth: { type: Number, default: -1 },
    id: { type: String, default: '' },
    customTitleClass: { type: String, default: '' },
    titleOverflow: { type: Boolean, default: false },
    title: { type: String, default: '' },
    selected: { type: Boolean, default: false },
    disabled: { type: Boolean, default: false },
    removeable: { type: Boolean, default: false },
    show: { type: Boolean, default: true }
};
export type TabPageProps = ExtractPropTypes<typeof tabPageProps>;
