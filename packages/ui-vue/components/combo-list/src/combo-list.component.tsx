import { defineComponent, ref, SetupContext } from 'vue';
import { comboListProps, ComboListProps } from './combo-list.props';
import { ButtonEdit } from '../../button-edit';
import { ViewType } from './types';
import { groupIcon } from './const';
import { useState } from './composition/use-state';
import { useComboList } from './composition/use-combo-list';
import FOptions from './components/options';

export default defineComponent({
  name: 'FComboList',
  props: comboListProps,
  emits: [
    'clear'
  ],
  inheritAttrs: false,
  setup(props: ComboListProps, context: SetupContext) {
    const modelValue = ref(props.modelValue);
    // is panel visible
    const { isPanelVisible, onClear, onButtonClick } = useComboList(props, context, modelValue);
    const [displayText, setDisplayText] = useState(props.displayText);
    return () => {
      return (
        <>
          {/** main component */}
          <ButtonEdit
            class={{ active: isPanelVisible }}
            disable={props.disabled}
            readonly={props.readonly}
            forcePlaceholder={props.forcePlaceholder}
            editable={props.editable}
            buttonContent={groupIcon}
            placeholder={props.placeholder}
            enableClear={props.enableClear}
            maxLength={props.maxLength}
            style="display:block"
            modelValue={displayText.value}
            onClear={onClear}
            onClickButton={onButtonClick}
          />
          {/** tag area */}
          {props.viewType === ViewType.Tag && (
            <div class="f-cmp-inputgroup">
              <div class="input-group">
                <div class="form-control f-cmp-inputgroup--multi-wrapper multi-more">
                  <div class="multi--content">
                    <span class="multi--item">
                      <i class="f-icon multi--close"></i>
                    </span>
                  </div>
                  <div class="multi--more">
                    <i class="f-icon multi--more-icon"></i><span class="multi--more-text"></span>
                  </div>
                </div>
                <div class="input-group-append">
                  <span class="input-group-text input-group-clear ng-star-inserted" style="display: none;">
                    <i class="f-icon modal_close"></i>
                  </span>
                  <span class="input-group-text ng-star-inserted">
                    <span class="f-icon f-icon-arrow-60-down"></span>
                  </span>
                </div>
              </div>
            </div>
          )}
          {/** panel area */}
          {isPanelVisible.value && (
            <div class="comboPanel f-area-hide" style="z-index: 99999">
              show
              <FOptions options={props.data}></FOptions>
            </div>
          )}
        </>
      );
    };
  }
});