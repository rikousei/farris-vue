import { ref, Ref, SetupContext } from "vue";
import { ComboListProps } from "../combo-list.props";
import { ModelValue } from "../types";

export function useComboList(props: ComboListProps, context: SetupContext, modelValue: Ref<ModelValue>) {
    const isPanelVisible = ref(false);
    //#region events
    function onClear($event: Event) {
        modelValue.value = '';
        context.emit('clear');
    }
    function onButtonClick($event: Event) {
        isPanelVisible.value = !isPanelVisible.value;
    }
    //#endregion
    return {
        isPanelVisible,
        onClear,
        onButtonClick
    };
}