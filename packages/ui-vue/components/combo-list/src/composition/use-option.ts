import { computed, Ref, SetupContext } from "vue";
import { IUseOption } from "../types";
import { OptionProps } from "../combo-list.props";

export function useOption(props: OptionProps, context: SetupContext): IUseOption {
    const name = computed(() => {
        return props.name || props.value;
    });
    return {
        name
    };
}