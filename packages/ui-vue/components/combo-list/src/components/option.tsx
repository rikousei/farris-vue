import { defineComponent, SetupContext } from 'vue';
import { optionProps, OptionProps } from '../combo-list.props';
import { useOption } from '../composition/use-option';

export default defineComponent({
    name: 'FOption',
    props: optionProps,
    emits: [],
    inheritAttrs: false,
    setup(props: OptionProps, context: SetupContext) {
        const { name } = useOption(props, context);
        return () => {
            return (
                <li style="cursor: default;" class="list-group-item list-group-item-action">
                    {context.slots?.default ? context.slots.default() : name.value}
                </li>
            );
        };
    }
});