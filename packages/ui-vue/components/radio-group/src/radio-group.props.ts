import { ExtractPropTypes, PropType } from 'vue';
import { Radio } from './composition/types';
import { Observable, Subscription } from 'rxjs';

export const radioGroupProps = {
    /**
     * 组件标识
     */
    id: String,
    /**
     * 组件名称
     */
    name: { type: String, default: '' },
    /**
     * 单选组枚举数组
     */
    enumData: Array<Radio>,
    /**
     * 枚举数组中展示文本的key值。
     */
    textField: { type: String, default: 'name' },
    /**
     * 枚举数组中枚举值的key值。
     */
    valueField: { type: String, default: 'value' },
    /**
     * 组件是否水平排列
     */
    horizontal: { type: Boolean, default: false },
    /**
     * 禁用组件，不允许切换单选值
     */
    disabled: { type: Boolean, default: false },

    /**
     * 组件值
     */
    modelValue: { type: String, default: '' },

    /**
     * 输入框Tab键索引
     */
    tabIndex: Number,
};

export type RadioGroupProps = ExtractPropTypes<typeof radioGroupProps>;
