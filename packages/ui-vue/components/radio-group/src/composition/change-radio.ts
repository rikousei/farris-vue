import { ChangeRadio, Radio } from './types';
import { computed, Ref, SetupContext } from 'vue';
import { RadioGroupProps } from '../radio-group.props';

export function changeRadio(props: RadioGroupProps, context: SetupContext, modelValue: Ref<string>): ChangeRadio {

    const canChangeRadioButton = computed(() => !props.disabled);
    const enumData = computed(() => props.enumData || []);

    function getValue(item: Radio): any {
        return item[props.valueField];
    };

    function getText(item: Radio): any {
        return item[props.textField];
    };

    function onClickRadio(item: Radio, $event: Event) {
        if (canChangeRadioButton.value) {
            const newValue = getValue(item);
            if (modelValue.value !== newValue) {
                modelValue.value = newValue;

                context.emit('changeValue', newValue);

                context.emit('update:modelValue', newValue);
            }
        }
        $event.stopPropagation();
    }

    return {
        enumData,
        getValue,
        getText,
        onClickRadio
    };
}
