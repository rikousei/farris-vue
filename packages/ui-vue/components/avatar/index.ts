import type { App } from 'vue';
import Avatar from './src/avatar.component';

export * from './src/avatar.props';

export { Avatar };

export default {
    install(app: App): void {
        app.component(Avatar.name, Avatar);
    }
};
