import type { App } from 'vue';
import Tooltip from './src/tooltip.component';
import TooltipDirective from './src/tooltip.directive';

export * from './src/tooltip.props';

export { Tooltip, TooltipDirective };

export default {
    install(app: App): void {
        app.component(Tooltip.name, Tooltip);
        app.directive('tooltip', TooltipDirective);
    }
};
