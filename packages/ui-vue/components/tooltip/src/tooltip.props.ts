import { ExtractPropTypes, PropType } from 'vue';

export type TooltipPlacement =
    | 'top'
    | 'top-left'
    | 'top-right'
    | 'bottom'
    | 'bottom-left'
    | 'bottom-right'
    | 'right'
    | 'right-top'
    | 'right-bottom'
    | 'left'
    | 'left-top'
    | 'left-bottom';

export const tooltipProps = {
    content: { type: String },
    width: { type: Number },
    customClass: { type: String },
    placement: { type: String as PropType<TooltipPlacement>, default: 'top' },
    reference: {
        type: Object as PropType<HTMLElement>,
        require: true,
    },
    horizontalRelative: { type: String, defatul: "" },
    verticalRelative: { type: String, defatul: "" },
};
export type TooltipProps = ExtractPropTypes<typeof tooltipProps>;
