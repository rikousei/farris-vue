import { reactive, createApp, onUnmounted } from 'vue';
import type { App } from 'vue';
import { NotifyProps } from './notify.props';
import Notify from './notify.component';

function initInstance(props: NotifyProps, content?: string): App {
    const container = document.createElement('div');
    container.style.display = 'contents';
    const app: App = createApp({
        setup() {
            onUnmounted(() => {
                document.body.removeChild(container);
            });
            return () => <Notify {...props} onClose={app.unmount}></Notify>;
        }
    });
    document.body.appendChild(container);
    app.mount(container);
    return app;
}

export default class NotifyService {
    static show(options: NotifyProps): void {
        const props: NotifyProps = reactive({
            ...options
        });
        initInstance(props);
    }
}
