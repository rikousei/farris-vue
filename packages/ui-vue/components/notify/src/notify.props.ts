import { ExtractPropTypes, PropType } from 'vue';

export type NotifyPosition = 'bottom-right' | 'bottom-left' | 'top-right' | 'top-left' | 'top-center' | 'bottom-center' | 'center-center';

export type ToastyAnimate =
    | 'bounceInRight'
    | 'bounceInLeft'
    | 'bounceInRight'
    | 'bounceInLeft'
    | 'bounceInDown'
    | 'bounceInUp'
    | 'bounceIn'
    | 'fadeIn';

export type NotifyTheme = 'default' | 'material' | 'bootstrap';

export interface NotifyButton {
    customClass?: string;
    text: string;
    disable?: boolean;
    onClick?: ($event: Event, component: any) => any;
}

export interface NotifyData {
    type: string;
    title?: string;
    msg?: string;
    /** 按钮列表模板 */
    buttons?: Array<NotifyButton>;
    showClose?: boolean;
    theme?: string;
    timeout?: number;
    onAdd?: () => void;
    onRemove?: () => void;
    id?: number | string;
}

// export interface NotifyData extends NotifyOptions {}

export const notifyProps = {
    limit: { type: Number, default: 5 },
    showCloseButton: { type: Boolean, default: true },
    position: { type: String as PropType<NotifyPosition>, default: 'top-center' },
    timeout: { type: Number, default: 3000 },
    theme: { type: String as PropType<NotifyTheme>, default: 'bootstrap' },
    left: { type: Number },
    right: { type: Number },
    top: { type: Number },
    bottom: { type: Number },
    id: { type: String },
    animate: { type: String as PropType<ToastyAnimate>, default: 'fadeIn' },
    toasts: { type: Array<NotifyData> },
    options: { type: Object as PropType<NotifyData> }
};
export type NotifyProps = ExtractPropTypes<typeof notifyProps>;
