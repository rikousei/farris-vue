import type { App } from 'vue';
import Notify from './src/notify.component';
import Toast from './src/components/toast.component';
import NotifyService from './src/notify.service';

export * from './src/notify.props';
export * from './src/components/toast.props';

export { Notify, NotifyService, Toast };

export default {
    install(app: App): void {
        app.component(Notify.name, Notify);
        app.component(Toast.name, Toast);
        app.provide('NotifyService', NotifyService);
    }
};
