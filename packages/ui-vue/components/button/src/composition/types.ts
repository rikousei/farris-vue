export interface UseButton {
    /**
    * 附加按钮的Class
    */
    // buttonClass: ComputedRef<Record<string, boolean | undefined>>;
    /**
     * 点击附加按钮事件响应函数
     */
    onClickButton: ($event: Event) => void;

}
