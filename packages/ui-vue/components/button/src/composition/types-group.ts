export interface UseButtonGroup {
    showPanel: any;
    clickEvent: ($event: Event) => void;
    toggle: ($event: Event, btn: any) => void;
    getRealPlacement: (pment: any) => void;
    bindMenuMouseenter: (this: any) => void;
    unbindMenuMouseleave: (this: any) => void;
    bindMenuMouseleave: (this: any) => void;
}
