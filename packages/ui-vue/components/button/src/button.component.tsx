import { defineComponent, computed } from 'vue';
import type { SetupContext } from 'vue';
import { buttonProps, ButtonProps } from './button.props';
import { useButton } from './composition/use-button';

export default defineComponent({
    name: 'FButton',
    props: buttonProps,
    emits: ['click'],
    setup(props: ButtonProps, context: SetupContext) {
        const { onClickButton } = useButton(props, context);
        return () => (
            <button class={(props.size === 'large' ? 'btn-lg ' : 'btn-sm ') + 'btn ' + (props.type ? 'btn-' + props.type + ' ' : '')}
                disabled={props.disabled}
                onClick={(event: MouseEvent) => onClickButton(event)} >
                {context.slots.default && context.slots.default()}
            </button >
        );
    },
});
