import { defineComponent, computed, ref, watch } from 'vue';
import type { SetupContext } from 'vue';
import { buttonGroupProps, ButtonGroupProps } from './button-group.props';
import { useButtonGroup } from './composition/use-button-group';
import { ElForm } from 'element-plus';

export default defineComponent({
    name: 'FButtonGroup',
    props: buttonGroupProps,
    emits: ['click', 'changeState', 'change', 'clickMenuOut'],
    setup(props: ButtonGroupProps, context: SetupContext) {
        /* 显示出来的按钮组 */
        const flatButtons = props.data && props.data.slice(0, props.count);
        const dpButtons = props.data && props.data.slice(props.count);
        const $dpMenu = ref<InstanceType<typeof ElForm>>(null);
        const $dpBtn = ref<InstanceType<typeof ElForm>>(null);
        const { showPanel, clickEvent, toggle, getRealPlacement, bindMenuMouseenter, unbindMenuMouseleave, bindMenuMouseleave } =
            useButtonGroup(props, context, $dpMenu, $dpBtn);
        getRealPlacement(props.placement);

        return () => (
            <div class="f-btn-group">
                <div class={((props.size === 'large' ? 'btn-group-lg' : 'btn-group-sm') + ' btn-group f-btn-group-links')}>
                    {
                        flatButtons.map((btn) => {
                            // (change)="changeEvent($event)"
                            return (
                                <div>
                                    {
                                        btn.icon &&
                                        <button id="btn.id"
                                            class={('btn btn-link btn-icontext' + (btn.disabled ? ' disabled' : ''))}
                                            onClick={(event: MouseEvent) => toggle(event, btn)}>
                                            <i class={"'f-icon '+ btn.icon?btn.icon:''"}></i>
                                        </button>
                                    }
                                    {
                                        !btn.icon &&
                                        <button id="btn.id"
                                            class={('btn ' + (btn.type ? 'btn-' + btn.type : 'btn-link') +
                                                ' ' + (btn.type && btn.type !== 'link' ? 'f-btn-ml' : '') +
                                                (btn.disabled ? ' disabled' : ''))}
                                            onClick={(event: MouseEvent) => toggle(event, btn)} >
                                            {btn?.text}
                                        </button>
                                    }
                                </div>
                            );
                        })
                    }
                </div>
                {
                    !dpButtons.length ? '' :
                        <div class="btn-group f-btn-group-dropdown" onMouseleave={(event: MouseEvent) => unbindMenuMouseleave()}>
                            <button ref={$dpBtn} type="button" class="f-btn-dropdown" onClick={(event: MouseEvent) => clickEvent(event)}>
                                <span class="f-icon f-icon-lookup"></span>
                            </button>
                            <div ref={$dpMenu} class={('dropdown-menu f-btn-group-dropdown-menu' + (showPanel.value ? ' show' : ''))}
                                style="position:fixed;"
                                onMouseenter={(event: MouseEvent) => bindMenuMouseenter()}
                                onMouseleave={(event: MouseEvent) => bindMenuMouseleave()}
                            >
                                {
                                    dpButtons.map((dpBtn) => {
                                        return (
                                            <div>
                                                {
                                                    <div>
                                                        {dpBtn.divider && <div class="dropdown-divider"></div>}
                                                        <li id="dpBtn.id" class={('dropdown-item' + (dpBtn.disabled ? ' disabled' : ''))}
                                                            onClick={(event: MouseEvent) => toggle(event, dpBtn)}>
                                                            {
                                                                dpBtn.icon &&
                                                                <i class={('f-icon dropdown-item-icon' + dpBtn.icon)}></i>
                                                            }
                                                            <span>{dpBtn?.text}</span>
                                                        </li >
                                                    </div>
                                                }
                                            </div>
                                        );

                                    })
                                }
                            </div >
                        </div >
                }
            </div >
        );
    }
});
