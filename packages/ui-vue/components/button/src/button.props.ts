import { ExtractPropTypes, PropType } from 'vue';

type ButtonType = 'primary' | 'warning' | 'danger' | 'success' | 'link' | 'secondary';
type SizeType = 'small' | 'large';

export const buttonProps = {
    /**
     * 组件标识
     */
    id: String,
    /**
     * 设置按钮类型
     */
    type: { type: String as PropType<ButtonType>, default: 'primary' },
    /**
     * 是否禁用
     */
    disabled: { type: Boolean, default: false },
    /**
     * 按钮尺寸
     */
    size: { type: String as PropType<SizeType>, default: 'small' },
    // /**
    //  * button上显示的文字
    //  */
    // text: { type: String }
};

export type ButtonProps = ExtractPropTypes<typeof buttonProps>;
