import { computed, defineComponent, ref, SetupContext, Teleport } from 'vue';
import { PopoverProps, popoverProps } from './popover.props';

import './popover.scss';

export default defineComponent({
    name: 'FPopover',
    props: popoverProps,
    emits: [],
    setup(props: PopoverProps, context: SetupContext) {

        const position = ref(props.position);

        const shouldShowTitle = computed(() => !!props.title);

        const popoverClass = computed(() => {
            const originPopover = `popover in popover-${position.value}`;
            const bsPopover = `bs-popover-${position.value}`;
            const popoverClassObject = {} as any;
            popoverClassObject[originPopover] = true;
            popoverClassObject[bsPopover] = true;
        });

        const popoverContainerClass = computed(() => ({
            'popover-content': true,
            'popover-body': true
        }));

        return () => {
            return (
                <Teleport to="body">
                    <div class={popoverClass.value}>
                        <div class="popover-arrow arrow"></div>
                        {shouldShowTitle.value && <h3 class="popover-title popover-header">{props.title}</h3>}
                        <div class={popoverContainerClass.value}>{context.slots.default && context.slots?.default()}</div>
                    </div>
                </Teleport>
            );
        };
    }
});
