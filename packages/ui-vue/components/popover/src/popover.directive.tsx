import { App, createApp, onUnmounted, reactive } from "vue";
import { PopoverProps } from "./popover.props";

const popoverDirective = {
    mounted: (element: HTMLElement, binding: Record<string, any>, vnode: any) => {
        let app: App | null;
        console.log('mounted popover directive');
    },
    unMounted: (element: HTMLElement, binding: Record<string, any>, vnode: any) => { }
};
export default popoverDirective;
