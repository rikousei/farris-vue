import { ExtractPropTypes, PropType } from 'vue';

export type PopoverPosition = 'top' | 'bottom' | 'left' | 'right' | 'auto';

export const popoverProps = {
    title: { type: String },
    position: { type: String as PropType<PopoverPosition>, default: 'top' }
};

export type PopoverProps = ExtractPropTypes<typeof popoverProps>;
