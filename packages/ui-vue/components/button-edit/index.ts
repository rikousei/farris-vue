import type { App } from 'vue';
import ButtonEdit from './src/button-edit.component';

export * from './src/button-edit.props';

export { ButtonEdit };

export default {
    install(app: App): void {
        app.component(ButtonEdit.name, ButtonEdit);
    }
};
