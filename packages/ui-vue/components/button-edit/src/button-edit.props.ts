import { ExtractPropTypes, PropType } from 'vue';

type TextAlignment = 'left' | 'center' | 'right';

export const buttonEditProps = {
    /**
     * 组件标识
     */
    id: String,
    /**
     * 扩展按钮显示内容，这是一段现在扩展按钮中的html标签
     */
    buttonContent: { type: String, default: '<i class="f-icon f-icon-lookup"></i>' },
    /**
     * 启用输入框自动完成功能
     */
    autoComplete: { type: Boolean, default: false },
    /**
     * 组件自定义样式
     */
    customClass: { type: String, default: '' },
    /**
     * 禁用组件，既不允许在输入框中录入，也不允许点击扩展按钮。
     */
    disable: { type: Boolean, default: false },
    /**
     * 允许在输入框中录入文本。
     */
    editable: { type: Boolean, default: true },
    /**
     * 显示清空文本按钮
     */
    enableClear: { type: Boolean, default: false },
    /**
     * 组件值
     */
    modelValue: { type: String, default: '' },
    /**
     * 将组件设置为只读，既不允许在输入框中录入，也不允许点击扩展按钮，但是允许复制输入框中的内容。
     */
    readonly: { type: Boolean, default: false },
    /**
     * 文本对齐方式
     */
    textAlign: { type: String as PropType<TextAlignment>, default: 'left' },
    /**
     * 禁用组件时，是否显示扩展按钮
     */
    showButtonWhenDisabled: { type: Boolean, default: false },

    /**
     * 显示输入框的标签
     */
    enableTitle: { type: Boolean, default: false },
    /**
     * 输入框类型
     */
    inputType: { type: String, default: 'text' },
    /**
     * 显示输入框提示信息
     */
    forcePlaceholder: { type: Boolean, default: false },
    /**
     * 输入框提示文本
     */
    placeholder: { type: String, default: '' },
    /**
     * 输入框最小长度
     */
    minLength: Number,
    /**
     * 输入框最大长度
     */
    maxLength: Number,
    /**
     * 输入框Tab键索引
     */
    tabIndex: Number,
};

export type ButtonEditProps = ExtractPropTypes<typeof buttonEditProps>;
