import { ExtractPropTypes } from 'vue';

export const accordionProps = {
    customClass: { type: Array<string>, default: [] },
    height: { type: Number },
    width: { type: Number },
    enableFold: { type: Boolean, default: true },
    expanded: { type: Boolean, default: false }
};
export type AccordionProps = ExtractPropTypes<typeof accordionProps>;
