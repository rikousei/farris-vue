import { isContext } from 'vm';
import { computed, defineComponent, SetupContext } from 'vue';
import { AccordionProps, accordionProps } from './accordion.props';

import './accordion.css';

export default defineComponent({
    name: 'FAccordion',
    props: accordionProps,
    emits: [],
    setup(props: AccordionProps, context: SetupContext) {
        const accordionStyle = computed(() => ({
            height: props.height ? `${props.height}px` : '',
            width: props.width ? `${props.width}px` : ''
        }));

        const accordionClass = computed(() => {
            const customClassArray = props.customClass;
            const accordionClassObject = {
                'farris-panel': true,
                'accordion': true
            };
            customClassArray.reduce<Record<string, unknown>>((classObject, classString) => {
                classObject[classString] = true;
                return classObject;
            }, accordionClassObject);
            return accordionClassObject;
        });

        return () => {
            return (
                <div class={accordionClass.value} style={accordionStyle.value}>
                    {context.slots.default && context.slots.default()}
                </div>
            );
        };
    }
});
