import { computed, defineComponent, onMounted, ref, SetupContext, toRefs, watch } from 'vue';
import { switchProps, SwitchProps } from './switch.props';

export default defineComponent({
    name: 'FSwitch',
    props: switchProps,
    emits: ['update:modelValue'],
    setup(props: SwitchProps, context: SetupContext) {
        const { disable, editable, square, size, checkedLabel, uncheckedLabel } = toRefs(props);

        const checked = ref(props.checked);
        const modelValue = ref(props.modelValue);

        function getBackgroundColor() {
            return '';
        }

        function getBorderColor() {
            return '';
        }

        function getSwitchColor() {
            return '';
        }

        const switchContainerClass = computed(() => ({
            switch: true,
            'f-cmp-switch': true,
            checked: checked.value,
            disabled: disable.value || !editable.value,
            squire: square.value,
            'switch-large': size.value === 'large',
            'switch-medium': size.value === 'medium',
            'switch-small': size.value === 'small'
        }));

        const switchContainerStyle = computed(() => ({
            outline: 'none',
            'backgroud-color': getBackgroundColor(),
            'border-color': getBorderColor()
        }));

        const smallStyle = computed(() => ({
            background: getSwitchColor()
        }));

        const shouldShowSwitch = computed(() => {
            // checkedLabel || uncheckedLabel
            return checkedLabel?.value || uncheckedLabel?.value;
        });

        function updateChecked($event: any, emitClick = true) {
            if (disable.value || !editable.value) {
                return;
            }
            checked.value = !checked.value;
            modelValue.value = checked.value;
            context.emit('update:modelValue', checked.value);
        }

        function onToggle($event: MouseEvent) {
            updateChecked($event);
        }

        watch(
            () => props.modelValue,
            (value: boolean) => {
                checked.value = value;
            }
        );

        onMounted(() => {
            checked.value = props.modelValue;
        });

        return () => {
            return (
                <>
                    <span
                        tabindex="0"
                        role="button"
                        class={switchContainerClass.value}
                        style={switchContainerStyle.value}
                        onClick={onToggle}>
                        {shouldShowSwitch.value && (
                            <span class="switch-pane">
                                <span class="switch-label-checked">{checkedLabel?.value}</span>
                                <span class="switch-label-unchecked">{uncheckedLabel?.value}</span>
                            </span>
                        )}

                        <small style={smallStyle.value}>{context.slots.default && context.slots.default()}</small>
                    </span>
                </>
            );
        };
    }
});
