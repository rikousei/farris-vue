import type { App } from 'vue';
import CheckBoxGroup from './src/checkbox-group.component';

export * from './src/checkbox-group.props';

export { CheckBoxGroup };

export default {
    install(app: App): void {
        app.component(CheckBoxGroup.name, CheckBoxGroup);
    }
};
